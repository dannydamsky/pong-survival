-- -----------------------------------------------------------------------------------
-- menu.lua
-- -----------------------------------------------------------------------------------
local widget = require("widget")
local composer = require("composer")
local scene = composer.newScene()
local physics = require("physics")
physics.start()
physics.setGravity(0, 0);

-- Local references to display properties
local contentHeight = display.contentHeight
local contentWidth = display.contentWidth
local contentCenterY = display.contentCenterY
local contentCenterX = display.contentCenterX

-- Min/Max Ball alignments
local startAlignment = 20
local endAlignment = contentWidth - startAlignment

-- Constants
local staticObjectProperties = {density = 1, friction = 0, bounce = 1}
local dynamicBallProperties = {radius = 16, bounce = 1}

-- Display groups
local backgroundGroup
local mainGroup

-- Display objects
local topWall
local leftWall
local rightWall
local bottomWall
local pongBalls = {}
local playNormalGameButton
local highScoresButton

local backgroundMusic

-- Creates a background gradient
local function createBackground()
    background = display.newRect(backgroundGroup, contentCenterX, contentCenterY, contentWidth, contentHeight);
    background.fill = {
        type = "gradient",
        color1 = {0.24, 0.15, 0.58},
        color2 = {0.06, 0.0375, 0.145},
        direction = "down"
    }
end -- createBackground()

-- Creates 4 walls surrounding the objects in the game.
local function createWalls()
    topWall = display.newRect(backgroundGroup, contentCenterX, 0, contentWidth, 1)
    leftWall = display.newRect(backgroundGroup, 0, contentCenterY, 1, contentHeight)
    rightWall = display.newRect(backgroundGroup, contentWidth, contentCenterY, 1, contentHeight)
    bottomWall = display.newRect(backgroundGroup, contentCenterX, contentHeight, contentWidth, 1)
    topWall.fill = nil
    leftWall.fill = nil
    rightWall.fill = nil
    bottomWall.fill = nil
    physics.addBody(topWall, "static", staticObjectProperties)
    physics.addBody(leftWall, "static", staticObjectProperties)
    physics.addBody(rightWall, "static", staticObjectProperties)
    physics.addBody(bottomWall, "static", staticObjectProperties)
end -- createWalls()

--[[
Randomly generates properties for the new pong ball.
Returns X, Y, VelocityX, VelocityY
]]
local function generatePongBallProps()
    local whereFrom = math.random(3)
    if (whereFrom == 1) then -- left
        return startAlignment, math.random(500), 320, 160
    elseif (whereFrom == 2) then -- top
        return math.random(contentWidth), startAlignment, 0, 320
    else -- right
        return endAlignment, math.random(500), -320, 160
    end -- if
end -- generatePongBallProps()

-- Creates a new pong ball and sets it in motion
local function createPongBall(index)
    local x, y, vX, vY = generatePongBallProps()
    local newBall = display.newImageRect(backgroundGroup, "images/ball.png", 32, 32)
    newBall.x = x
    newBall.y = y
    newBall.myName = ballName
    physics.addBody(newBall, "dynamic", dynamicBallProperties);
    newBall:setLinearVelocity(vX, vY)
    pongBalls[index] = newBall
end -- createPongBall()

-- Generates 10 pong balls
local function generatePongBalls()
    createPongBall(1)
    createPongBall(2)
    createPongBall(3)
    createPongBall(4)
    createPongBall(5)
    createPongBall(6)
    createPongBall(7)
    createPongBall(8)
    createPongBall(9)
    createPongBall(10)
end -- generatePongBalls()

local function gotoNormalGame()
    composer.gotoScene("scenes.normalgame", {time = 400, effect = "zoomInOutFade"})
end -- gotoNormalGame()

local function gotoHighScores()
    composer.gotoScene("scenes.highscores", {time = 400, effect = "crossFade"})
end -- gotoHighScores()

-- Creates a title from image
local function createTitle()
    local title = display.newImageRect(mainGroup, "images/title-main.png", 700, 102)
    title.x = contentCenterX
    title.y = contentCenterY - 200
end -- createTitle()

-- Callback from PlayNormalGame button
local function onPlayNormalGameButtonTouch(event)
    if (event.phase == "ended") then
        gotoNormalGame()
    end -- if
end -- onPlayNormalGameButtonTouch()

-- Creates the PlayNormalGame button and registers a callback
local function createPlayNormalGameButton()
    playNormalGameButton = widget.newButton{
        label = "Play",
        onEvent = onPlayNormalGameButtonTouch,
        emboss = true,
        -- Properties for a rounded rectangle button
        shape = "roundedRect",
        width = 400,
        height = 100,
        cornerRadius = 16,
        fillColor = {default = {0.263, 0.627, 0.278, 1}, over = {0.18, 0.49, 0.196, 1}},
        strokeColor = {default = {0, 0, 0, 0.5}, over = {0, 0, 0, 1}},
        labelColor = {default = {1, 1, 1}, over = {1, 1, 1, 0.5}},
        fontSize = 36,
        strokeWidth = 3
    }
    mainGroup:insert(playNormalGameButton)
    playNormalGameButton.x = contentCenterX
    playNormalGameButton.y = contentCenterY
end -- createPlayButton()

-- Callback from HighScores button
local function onHighScoresButtonTouch(event)
    if (event.phase == "ended") then
        gotoHighScores()
    end -- if
end -- onHighScoresButtonTouch()

-- Creates the HighScores button and registers a callback
local function createHighScoresButton()
    playNormalGameButton = widget.newButton{
        label = "High Scores",
        onEvent = onHighScoresButtonTouch,
        emboss = true,
        -- Properties for a rounded rectangle button
        shape = "roundedRect",
        width = 400,
        height = 100,
        cornerRadius = 16,
        fillColor = {default = {0.008, 0.533, 0.82, 1}, over = {0.004, 0.341, 0.608, 1}},
        strokeColor = {default = {0, 0, 0, 0.5}, over = {0, 0, 0, 1}},
        labelColor = {default = {1, 1, 1}, over = {1, 1, 1, 0.5}},
        fontSize = 36,
        strokeWidth = 3
    }
    mainGroup:insert(playNormalGameButton)
    playNormalGameButton.x = contentCenterX
    playNormalGameButton.y = contentCenterY + 200
end -- createPlayButton()

-- Creates two display groups and returns them
local function createTwoDisplayGroups(sceneGroup)
    local a, b = display.newGroup(), display.newGroup()
    sceneGroup:insert(a)
    sceneGroup:insert(b)
    return a, b
end -- createThreeDisplayGroups()

--[[
This function removes one of the pong balls that are remaining
on the screen after the game has ended.
]]
local function removePongBall(index)
    ball = pongBalls[index]
    if (ball) then
        display.remove(ball)
        ball = nil
        pongBalls[index] = nil
    end -- if
end -- removePongBall()

--[[
This function removes all the pong balls that are remaining
on the screen after the scene is hidden.
]]
local function removePongBalls()
    removePongBall(1)
    removePongBall(2)
    removePongBall(3)
    removePongBall(4)
    removePongBall(5)
    removePongBall(6)
    removePongBall(7)
    removePongBall(8)
    removePongBall(9)
    removePongBall(10)
end -- removePongBalls()

--[[
This function removes all buttons that are
on the screen.
]]
local function removeButtons()
    playNormalGameButton:removeSelf()
    playNormalGameButton = nil
end -- removeButtons()

-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------
function scene:create(event)
    local sceneGroup = self.view
    physics.pause()
    backgroundGroup, mainGroup = createTwoDisplayGroups(sceneGroup)
    createBackground()
    createWalls()
    createTitle()
    createPlayNormalGameButton()
    createHighScoresButton()
    backgroundMusic = audio.loadStream("audio/background-music-main.wav")
end -- create()


function scene:show(event)
    local sceneGroup = self.view
    local phase = event.phase
    
    if (phase == "will") then
        generatePongBalls()
    elseif (phase == "did") then
        physics.start()
        audio.play(backgroundMusic, {channel = 1, loops = -1});
    end
end -- show()


function scene:hide(event)
    local sceneGroup = self.view
    local phase = event.phase
    
    if (phase == "will") then
        -- Do nothing
    elseif (phase == "did") then
        physics.pause()
        removePongBalls()
        audio.stop(1)
    end
end -- hide()


function scene:destroy(event)
    local sceneGroup = self.view
    backgroundGroup:removeSelf()
    mainGroup:removeSelf()
    audio.dispose(backgroundMusic)
    backgroundMusic = nil
    backgroundGroup = nil
    mainGroup = nil
    topWall = nil
    bottomWall = nil
    leftWall = nil
    rightWall = nil
    playNormalGameButton = nil
end -- destroy()


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destroy", scene)
-- -----------------------------------------------------------------------------------
return scene
