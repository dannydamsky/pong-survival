-- -----------------------------------------------------------------------------------
-- normalgame.lua
-- -----------------------------------------------------------------------------------
require("data.model.highscore")
local composer = require("composer")
local scene = composer.newScene()
local physics = require("physics")
physics.start()
physics.setGravity(0, 0);

-- Local references to display properties
local contentHeight = display.contentHeight
local contentWidth = display.contentWidth
local contentCenterY = display.contentCenterY
local contentCenterX = display.contentCenterX

-- Min/Max Ball alignments
local startAlignment = 20
local endAlignment = contentWidth - startAlignment

-- Min/Max Brick alignments
local brickBoundsMinX = 76
local brickBoundsMaxX = display.contentWidth - brickBoundsMinX
local brickBoundsY = contentHeight - 100

-- Game utilities
local gameLoopTimer
local ballsCount = 0
local timeTillNextBallInSeconds = 3

-- Constants
local staticObjectProperties = {density = 1, friction = 0, bounce = 1}
local dynamicBallProperties = {radius = 16, bounce = 1}
local ballName = "ball"

-- Display groups
local backgroundGroup
local mainGroup
local uiGroup

-- Display objects
local background
local topWall
local leftWall
local rightWall
local bottomWall
local pongBalls = {}
local brick
local ballsText
local nextBallText

-- Audio tracks
local backgroundMusic
local bumpSound

-- Properties for optimisation
local recreateScene = false
local gameStopped = false

-- Creates a background gradient
local function createBackground()
    background = display.newRect(backgroundGroup, contentCenterX, contentCenterY, contentWidth, contentHeight);
    background.fill = {
        type = "gradient",
        color1 = {0.58, 0.15, 0.24},
        color2 = {0.145, 0.0375, 0.06},
        direction = "down"
    }
end -- createBackground()

-- Creates 4 walls surrounding the objects in the game.
local function createWalls()
    topWall = display.newRect(mainGroup, contentCenterX, 0, contentWidth, 1)
    leftWall = display.newRect(mainGroup, 0, contentCenterY, 1, contentHeight)
    rightWall = display.newRect(mainGroup, contentWidth, contentCenterY, 1, contentHeight)
    bottomWall = display.newRect(mainGroup, contentCenterX, contentHeight, contentWidth, 1)
    topWall.fill = nil
    leftWall.fill = nil
    rightWall.fill = nil
    bottomWall.fill = nil
    topWall.myName = "topWall"
    leftWall.myName = "leftWall"
    rightWall.myName = "rightWall"
    bottomWall.myName = "bottomWall"
    physics.addBody(topWall, "static", staticObjectProperties)
    physics.addBody(leftWall, "static", staticObjectProperties)
    physics.addBody(rightWall, "static", staticObjectProperties)
    physics.addBody(bottomWall, "static", staticObjectProperties)
end -- createWalls()

--[[
Randomly generates properties for the new pong ball.
Returns X, Y, VelocityX, VelocityY
]]
local function generatePongBallProps()
    local whereFrom = math.random(3)
    if (whereFrom == 1) then -- left
        return startAlignment, math.random(500), math.random(320, 960), math.random(160, 480)
    elseif (whereFrom == 2) then -- top
        return math.random(contentWidth), startAlignment, math.random(-320, 320), math.random(320, 960)
    else -- right
        return endAlignment, math.random(500), math.random(-960, -320), math.random(160, 480)
    end -- if
end -- generatePongBallProps()

-- Creates a new pong ball and sets it in motion
local function createPongBall()
    local x, y, vX, vY = generatePongBallProps()
    local newBall = display.newImageRect(mainGroup, "images/ball.png", 32, 32)
    newBall.x = x
    newBall.y = y
    newBall.myName = ballName
    physics.addBody(newBall, "dynamic", dynamicBallProperties);
    newBall:setLinearVelocity(vX, vY)
    pongBalls[#pongBalls + 1] = newBall
end -- createPongBall()

--[[
Gets called when the brick began movement (by user)
Sets focus on the brick and saves the touch offset.
]]
local function onBrickMoveBegan(event, brick)
    display.currentStage:setFocus(brick);
    brick.touchOffsetX = event.x - brick.x;
end -- onBrickMoveBegan()

--[[
Gets called when the movement of the brick continues (by user)
This function is in charge of moving the brick.
]]
local function onBrickMoveContinued(event, brick)
    -- Move the ship to the new touch position
    local currentX = event.x - (brick.touchOffsetX or 0);
    if (currentX < brickBoundsMinX) then
        brick.x = brickBoundsMinX
    elseif (currentX > brickBoundsMaxX) then
        brick.currentX = brickBoundsMaxX
    else
        brick.x = currentX
    end -- if
end -- onBrickMoveContinued()

--[[
Gets called when the movement of the brick stopped (by user)
The function removes the focus from the stage.
]]
local function onBrickMoveEnded()
    display.currentStage:setFocus(nil)
end -- onBrickMoveEnded()

-- Gets called when a brick movement callback was triggered.
local function onBrickMove(event)
    local brick = event.target
    local phase = event.phase
    
    if (phase == "began") then
        onBrickMoveBegan(event, brick)
    elseif (phase == "moved") then
        onBrickMoveContinued(event, brick)
    elseif (phase == "ended" or phase == "cancelled") then
        onBrickMoveEnded()
    end -- if
    
    return true
end -- onBrickMove()

--[[
Creates the brick (platform) at the bottom of the screen and sets an event listener
that listens for touch events
]]
local function createBottomBrick()
    brick = display.newImageRect(mainGroup, "images/brick.png", 144, 36)
    brick.x = contentCenterX
    brick.y = brickBoundsY
    brick.myName = "brick"
    physics.addBody(brick, "static", staticObjectProperties)
    brick:addEventListener("touch", onBrickMove);
end -- createBottomBrick()

-- Returns text indicating the amount of balls on screen.
local function getBallsText()
    return ballsCount .. " balls"
end -- getBallsText()

-- Returns text indication when the next ball will appear on screen
local function getNextBallText()
    return "Next ball in " .. timeTillNextBallInSeconds .. " seconds"
end -- getNextBallText()

-- Create text display objects
local function createGameTexts()
    ballsText = display.newText(uiGroup, getBallsText(), 100, 80, native.systemFont, 24);
    nextBallText = display.newText(uiGroup, getNextBallText(), 500, 80, native.systemFont, 24);
end -- createGameText()

-- Updates the text of all the text display objects
local function updateGameTexts()
    ballsText.text = getBallsText()
    nextBallText.text = getNextBallText()
end -- updateGameTexts()

-- Loads the audio files that'll be used with this scene
local function loadAudioFiles()
    backgroundMusic = audio.loadStream("audio/background-music-game.wav")
    bumpSound = audio.loadSound("audio/bump.wav")
end -- loadAudioFiles()

-- Creates three display groups and returns them
local function createThreeDisplayGroups(sceneGroup)
    local a, b, c = display.newGroup(), display.newGroup(), display.newGroup()
    sceneGroup:insert(a)
    sceneGroup:insert(b)
    sceneGroup:insert(c)
    return a, b, c
end -- createThreeDisplayGroups()

--[[
This functions gets run in a loop by a game timer.
It's in charge of updating the texts on screen with relevant info as well as
creating a new pong ball every 10 seconds.
]]
local function gameLoop()
    if (timeTillNextBallInSeconds == 1) then
        createPongBall()
        ballsCount = ballsCount + 1
        timeTillNextBallInSeconds = 10
    else
        timeTillNextBallInSeconds = timeTillNextBallInSeconds - 1
    end -- if
    updateGameTexts()
end -- gameLoop()

--[[
Ends the game, updates the score and returns to the main screen
]]
local function endGame()
    if (not gameStopped) then
        dbHelper:insertHighScore(HighScore:new(nil, ballsCount))
        composer.gotoScene("scenes.menu", {time = 400, effect = "crossFade"})
        gameStopped = true
    end
end -- endGame()

--[[
This function takes one display object as a parameter and detects its type.
The function returns references to display objects, only one of them will not be nil.
]]
local function detectOtherObject(object)
    local bottomWallReference, leftWallReference, rightWallReference, topWallReference, brickReference
    if (object.myName == bottomWall.myName) then
        bottomWallReference = object
    elseif (object.myName == leftWall.myName) then
        leftWallReference = object
    elseif (object.myName == rightWall.myName) then
        rightWallReference = object
    elseif (object.myName == topWall.myName) then
        topWallReference = object
    elseif (object.myName == brick.myName) then
        brickReference = object
    end -- if
    return bottomWallReference, leftWallReference, rightWallReference, topWallReference, brickReference
end -- detectOtherObject()

--[[
This function takes two display objects as parameters and detects their type.
The function returns references to display objects, only two of them will not be nil.
]]
local function detectCollidingObjects(firstObject, secondObject)
    local ballReference, bottomWallReference, leftWallReference, rightWallReference, topWallReference, brickReference
    if (firstObject.myName == ballName) then
        ballReference = firstObject
        bottomWallReference, leftWallReference, rightWallReference, topWallReference, brickReference = detectOtherObject(secondObject)
    elseif (secondObject.myName == ballName) then
        ballReference = secondObject
        bottomWallReference, leftWallReference, rightWallReference, topWallReference, brickReference = detectOtherObject(firstObject)
    end -- if
    return ballReference, bottomWallReference, leftWallReference, rightWallReference, topWallReference, brickReference
end -- detectCollidingObjects()

--[[
This function gets called by the global event listener callback of "collision"
]]
local function onCollision(event)
    if (event.phase == "began") then
        local ballReference, bottomWallReference, leftWallReference, rightWallReference, topWallReference, brickReference = detectCollidingObjects(event.object1, event.object2)
        if (ballReference and bottomWallReference) then
            timer.performWithDelay(500, endGame)
        end -- if
        audio.play(bumpSound)
    end -- if
end -- onCollision

--[[
This function removes all the pong balls that are remaining
on the screen after the game has ended.
]]
local function removePongBalls()
    for i = 1, #pongBalls do
        ball = pongBalls[i]
        if (ball) then
            display.remove(ball)
            ball = nil
            pongBalls[i] = nil
        end -- if
    end -- for
end -- removePongBalls()

-- This function resets the game after it has ended.
local function resetScene()
    Runtime:removeEventListener("collision", onCollision);
    physics.pause()
    audio.stop(1)
    removePongBalls()
    ballsCount = 0
    timeTillNextBallInSeconds = 3
    updateGameTexts()
end -- resetScene()

-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------
function scene:create(event)
    local sceneGroup = self.view
    physics.pause()
    backgroundGroup, mainGroup, uiGroup = createThreeDisplayGroups(sceneGroup)
    createBackground()
    createWalls()
    createBottomBrick()
    createGameTexts()
    loadAudioFiles()
    recreateScene = true
end -- create()


function scene:show(event)
    local sceneGroup = self.view
    local phase = event.phase
    
    if (phase == "will") then
        gameStopped = false
        brick.x = contentCenterX
    elseif (phase == "did") then
        physics.start();
        Runtime:addEventListener("collision", onCollision);
        gameLoopTimer = timer.performWithDelay(1000, gameLoop, 0);
        audio.play(backgroundMusic, {channel = 1, loops = -1})
    end -- if
end -- show()


function scene:hide(event)
    local sceneGroup = self.view
    local phase = event.phase
    
    if (phase == "will") then
        timer.cancel(gameLoopTimer)
    elseif (phase == "did") then
        audio.stop(1)
        resetScene()
    end -- if
end -- hide()


function scene:destroy(event)
    local sceneGroup = self.view
    audio.dispose(backgroundMusic)
    audio.dispose(bumpSound)
    backgroundMusic = nil
    bumpSound = nil
    backgroundGroup:removeSelf()
    mainGroup:removeSelf()
    uiGroup:removeSelf()
    backgroundGroup = nil
    mainGroup = nil
    uiGroup = nil
    background = nil
    topWall = nil
    leftWall = nil
    rightWall = nil
    bottomWall = nil
    pongBalls = nil
    brick = nil
    ballsText = nil
    nextBallText = nil
end -- destroy()


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destroy", scene)
-- -----------------------------------------------------------------------------------
return scene
