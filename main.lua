-- -----------------------------------------------------------------------------------
-- main.lua
-- -----------------------------------------------------------------------------------
require("data.dbhelper")
local composer = require("composer")

-- initialize DbHelper singleton
dbHelper = DbHelper:new()

-- Enable immersive mode on Android devices and hide the status bar on others.
if (system.getInfo("platformName") == "Android") then
    local androidVersion = string.sub(system.getInfo("platformVersion"), 1, 3)
    if (androidVersion) then
        if (tonumber(androidVersion) >= 4.4) then
            native.setProperty("androidSystemUiVisibility", "immersiveSticky")
        else
            native.setProperty("androidSystemUiVisibility", "lowProfile")
        end -- if
    else
        display.setStatusBar(display.hiddenStatusBar)
    end -- if
end -- if

-- Seed the random number generator
math.randomseed(os.time())

-- Go to the menu screen
composer.gotoScene("scenes.menu", {time = 400, effect = "fade"})

-- Reserve channel 1 for background music
audio.reserveChannels(1)
audio.setVolume(0.5, {channel = 1})
