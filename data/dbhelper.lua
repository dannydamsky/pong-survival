-- -----------------------------------------------------------------------------------
-- dbhelper.lua
-- -----------------------------------------------------------------------------------
require("data.model.highscore")
require("data.model.setting")

-- -----------------------------------------------------------------------------------
-- local functions
-- -----------------------------------------------------------------------------------

local db

-- Returns an open database object
local function getDatabase()
    local sqlite = require("sqlite3")
    local path = system.pathForFile("pongsurvival.db", system.DocumentsDirectory)
    return sqlite.open(path)
end -- getDatabase()

-- Creates all of the tables for the database if they don't exist
local function createTables()
    db:exec([[
        CREATE TABLE IF NOT EXISTS SETTINGS (
            COLUMN_SETTING_KEY TEXT PRIMARY KEY,
            COLUMN_SETTING_VALUE TEXT
        );
        CREATE TABLE IF NOT EXISTS HIGHSCORES (
            COLUMN_HIGHSCORE_ID INTEGER PRIMARY KEY,
            COLUMN_HIGHSCORE_SCORE INTEGER
        );
    ]])
end -- createTables()

-- Listens for system events and closes the database on application exit.
local function onSystemEvent(event)
    if (event.type == "applicationExit") then
        db:close()
    end -- if
end -- onSystemEvent()

-- -----------------------------------------------------------------------------------
-- class functions
-- -----------------------------------------------------------------------------------

DbHelper = {}
DbHelper_mt = {__index = DbHelper}

function DbHelper:new()
    db = getDatabase()
    createTables()
    Runtime:addEventListener("system", onSystemEvent)
    local self = {}
    setmetatable(self, DbHelper_mt)
    return self
end -- new()

-- inserts another High Score into the database
function DbHelper:insertHighScore(highScore)
    db:exec("INSERT INTO HIGHSCORES (COLUMN_HIGHSCORE_SCORE) VALUES (" .. highScore.score .. ");")
end -- insertHighScore()

-- returns a list of high scores.
function DbHelper:getHighScores()
    local sql = "SELECT * FROM HIGHSCORES ORDER BY COLUMN_HIGHSCORE_SCORE DESC;"
    local highScores = {}
    local highScoresSize = 0
    for row in db:nrows(sql) do
        highScores[highScoresSize] = HighScore:new(row.COLUMN_HIGHSCORE_ID, row.COLUMN_HIGHSCORE_SCORE)
        highScoresSize = highScoresSize + 1
    end -- for
    return highScores
end -- getHighScores()

-- inserts or updates a setting in the database
function DbHelper:putSetting(setting)
    db:exec("INSERT INTO SETTINGS (COLUMN_SETTING_KEY, COLUMN_SETTING_VALUE) VALUES (" .. setting.key .. ", " .. setting.value .. ");")
end -- putSetting()

-- returns a setting associated with the given key
function DbHelper:getSettingByKey(key)
    local sql = "SELECT * FROM SETTINGS WHERE COLUMN_SETTING_KEY='" .. key .. "' LIMIT 1;"
    for row in db:nrows(sql) do
        return Setting:new(row.COLUMN_SETTING_KEY, row.COLUMN_SETTING_VALUE)
    end -- for
end -- getSettingByKey()

-- returns a list of settings
function DbHelper:getSettings()
    local settings = {}
    local settingsSize = 0
    for row in db:nrows("SELECT * FROM SETTINGS") do
        settings[settingsSize] = Setting:new(row.COLUMN_SETTING_KEY, row.COLUMN_SETTING_VALUE)
        settingsSize = settingsSize + 1
    end -- for
    return settings
end -- getSettings()

-- deletes all of the High Scores from the database
function DbHelper:deleteHighScores()
    db:exec("DELETE FROM HIGHSCORES;")
end -- deleteHighScores()

-- deletes all of the Settings from the database
function DbHelper:deleteSettings()
    db:exec("DELETE FROM SETTINGS;")
end -- deleteSettings()

-- deletes all rows stored in the database
function DbHelper:clearDatabase()
    db:exec("BEGIN TRANSACTION;")
    self.deleteHighScores()
    self.deleteSettings()
    db:exec("END TRANSACTION;")
end -- clearDatabase()