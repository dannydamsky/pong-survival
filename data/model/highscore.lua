-- -----------------------------------------------------------------------------------
-- highscore.lua
-- -----------------------------------------------------------------------------------
HighScore = {id = nil, score = 0}
HighScore_mt = {__index = HighScore}

function HighScore:new(id, score)
    local self = {}
    self.id = id
    self.score = score
    setmetatable(self, HighScore_mt)
    return self
end -- new()
