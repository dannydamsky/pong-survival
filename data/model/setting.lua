-- -----------------------------------------------------------------------------------
-- setting.lua
-- -----------------------------------------------------------------------------------
Setting = {key = nil, value = nil}
Setting_mt = {__index = Setting}

function Setting:new(key, value)
    local self = {}
    self.key = key
    self.value = value
    setmetatable(self, Setting_mt)
    return self
end -- new()